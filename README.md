# hashing emptyness !


### QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nn

This is the hash for an IPFS empty dir

* <https://kmufv4bhl96kdcwj5r09m36fz75gdzl6m1dsmfd58w6fwxlmnq57z.ipfs.dweb.link/>

```sh
rm -r empty
mkdir empty
qm=$(ipfs add -Q -r empty)
echo qm: $qm
kmu=$(ipfs cid format -f=%m -b=base36 $qm)
echo kmu: $kmu
```


### other form :

* [k2jmtxtlhjl3fhmgndf92e48by79ryjuvqp3y2qgehpao6v3lurvnmcv](https://k2jmtxtlhjl3fhmgndf92e48by79ryjuvqp3y2qgehpao6v3lurvnmcv.ipfs.dweb.link)
* f017000040a020801
* bafyaabakaieac
* [KSBX1HFH4SU9](https://ksbx1hfh4su9.ipfs.dweb.link/)
* zEwqqjyz8Qg
